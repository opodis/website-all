<?xml version="1.0" encoding="iso-8859-1"?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Opodis Web Site - Welcome</title>
<meta name="title" content="Welcome" />
<meta name="author" content="Administrator" />
<meta name="description" content="opodis" />
<meta name="keywords" content="opodis" />
<meta name="Generator" content="Joomla! - Copyright (C) 2005 - 2007 Open Source Matters. All rights reserved." />
<meta name="robots" content="index, follow" />
	<link rel="shortcut icon" href="http://cosy.univ-reims.fr/~opodis09/images/favicon.ico" />
	<meta http-equiv="Content-Type" content="text/html" charset=iso-8859-1" />

<link rel="alternate" title="Opodis Web Site" href="http://cosy.univ-reims.fr/~opodis09/index2.php?option=com_rss&no_html=1" type="application/rss+xml" />
<link rel="alternate" type="application/rss+xml" title="Opodis Web Site" href="http://cosy.univ-reims.fr/~opodis09/index.php?option=com_rss&feed=RSS2.0&no_html=1" />

<link rel="stylesheet" href="http://cosy.univ-reims.fr/~opodis09/templates/siteground71/css/template.css" type="text/css"/>

</head>
<body id="page_bg">

<a name="up" id="up"></a>

<div id="frame_bg">
	<div id="wrapper">
			<div id="whitebox">
				<table cellpadding="0" cellspacing="0" class="pill">
					<tr>
						<td class="pill_m">
							<div id="pillmenu">
								<div id="menu">
	<div class="menuc">
		<div id="topnavi">
			<ul>
			<li><a href='index.php?option=com_content&task=view&id=14&Itemid=30' class='current' ><span>Welcome</span></a></li><li><a href='index.php?option=com_content&task=view&id=13&Itemid=27'  ><span>Call for Papers</span></a></li><li><a href='index.php?option=com_content&task=view&id=12&Itemid=28'  ><span>Committee</span></a></li><li><a href='http://www.easychair.org/conferences/?conf=opodis09&Itemid=31'  ><span>Submission</span></a></li><li><a href='http://studia.complexica.net/&Itemid=32'  ><span>Studia Informatica</span></a></li>			</ul>
		</div>
	</div>
</div>							</div>
						</td>
					</tr>
				</table>
			</div>
	
			<div id="header">
				<div id="header_l">
					<div id="logo_bg">
						<div id="logo">
							<a class="logo" href="index.php">Opodis Web Site</a>
					<div id="topnews">
											</div>							
						</div>
						<div id="clr"></div>
					</div>
				</div>
			</div>
	</div>
	<div id="extras">
		<div id="search">
					</div>
		<div id="pathway">
			<span class="pathway">Welcome </span>		</div>
		<div id="clr"></div>
	</div>
	
	
						
			<div id="whitebox_m">
				<div id="area">
													<div id="leftcolumn" style="float:left;">
										<table cellpadding="0" cellspacing="0" class="moduletable">
					<tr>
				<th valign="top">
					Main Menu				</th>
			</tr>
					<tr>
			<td>
				
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr align="left"><td><a href="http://cosy.univ-reims.fr/~opodis09/index.php?option=com_content&amp;task=view&amp;id=14&amp;Itemid=30" class="mainlevel" id="active_menu">Welcome</a></td></tr>
<tr align="left"><td><a href="http://cosy.univ-reims.fr/~opodis09/index.php?option=com_content&amp;task=view&amp;id=13&amp;Itemid=27" class="mainlevel" >Call for Papers</a></td></tr>
<tr align="left"><td><a href="http://cosy.univ-reims.fr/~opodis09/index.php?option=com_content&amp;task=view&amp;id=12&amp;Itemid=28" class="mainlevel" >Committee</a></td></tr>
<tr align="left"><td><a href="http://www.easychair.org/conferences/?conf=opodis09" class="mainlevel" >Submission</a></td></tr>
<tr align="left"><td><a href="http://studia.complexica.net/" class="mainlevel" >Studia Informatica</a></td></tr>
<tr align="left"><td><a href="http://cosy.univ-reims.fr/~opodis09/index.php?option=com_content&amp;task=view&amp;id=15&amp;Itemid=33" class="mainlevel" >Annoucements</a></td></tr>
<tr align="left"><td><a href="http://cosy.univ-reims.fr/~opodis09/index.php?option=com_content&amp;task=view&amp;id=16&amp;Itemid=34" class="mainlevel" >Accepted Papers</a></td></tr>
<tr align="left"><td><a href="http://cosy.univ-reims.fr/~opodis09/Registration" class="mainlevel" >Registration</a></td></tr>
<tr align="left"><td><a href="http://cosy.univ-reims.fr/~opodis09/index.php?option=com_content&amp;task=view&amp;id=17&amp;Itemid=36" class="mainlevel" >Programm</a></td></tr>
<tr align="left"><td><a href="http://cosy.univ-reims.fr/~opodis09/index.php?option=com_content&amp;task=view&amp;id=18&amp;Itemid=37" class="mainlevel" >Local Accomodation</a></td></tr>
</table>			</td>
		</tr>
		</table>
										
							</div>
												
												<div id="maincolumn_full">
													<div class="nopad">
												<table class="contentpaneopen">
			<tr>
								<td class="contentheading" width="100%">
					Welcome									</td>
							</tr>
			</table>
			
		<table class="contentpaneopen">
				<tr>
			<td valign="top" colspan="2">
				<h1 align="center">13th International Conference On Principles Of DIstributed Systems&nbsp;</h1><h1 align="center">December 15-18, 2009</h1><h1 align="center">Nimes France</h1><h1 align="center">http://www.opodis.net&nbsp;</h1><p>&nbsp;</p><h1><font color="red"> Registrations are now open !!!<br /></font></h1><p>&nbsp;</p><p><strong>Important dates</strong><br />Submission EXTENDED deadline:&nbsp;&nbsp; July 25,&nbsp; 2009</p><p>Acceptance notification: September 15, 2009</p><p>Camera-ready copy due: September 29, 2009</p><h4 align="left">Invited Speakers&nbsp;</h4><p>M. Herlihy, Brown U., US</p><p>A-M. Kermarrec, IRISA Rennes, FR&nbsp;</p><p>&nbsp;</p><p>The conference will be held in the University of Nimes. </p><p>&nbsp;</p><div style="text-align: center"><a href="http://www.springer.com/computer/lncs?SGWID=0-164-0-0-0"><img src="images/M_images/lncs.jpg" alt=" " width="182" height="67" /></a> </div><p>&nbsp;</p>			</td>
		</tr>
				</table>

		<span class="article_seperator">&nbsp;</span>

									</div>
						</div>
												<div class="clr"></div>
				</div>
			</div>
			
			<div id="footer">
				<div id="footer_l">
					<div id="footer_r">

					</div>
				</div>
			</div>
			<div id="sgf"> 	Opodis Web Site, Powered by <a href="http://joomla.org/" class="sgfooter" target="_blank">Joomla!</a>; <a href="http://www.siteground.com/joomla-hosting/joomla-templates.htm" target="_blank" class="sgfooter">Joomla templates</a> by SG <a href="http://www.siteground.com/" target="_blank" class="sgfooter">web hosting</a>
 </div>
</div>
</div>
</body>
</html>
<!-- 1257643490 -->