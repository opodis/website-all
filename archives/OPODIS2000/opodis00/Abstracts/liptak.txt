We investigate the problem of broadcasting in a complete synchronous
network with dynamic edge faults. The faults may be loss of messages
only (omissions) or of arbitrary type (Byzantine faults).  In both
cases, broadcasting can be done in at most $5$ rounds, i.e. in
constant time, as long as the maximal number $\phi$ of faults per
round allows broadcasting at all. Furthermore, we establish upper
bounds on $\phi$ if an algorithm is to achieve broadcasting in $r$
rounds, for $r \in \{2,3,4,5\}.$ These bounds depend on the type of
faults occurring, and on the type of messages which can be sent
(arbitrary length, bit messages only, and bit messages where
processors always send). For each of these cases, we present
algorithms which are optimal, or (in one case) miss the maximal value
of $\phi$ by only $1$. All algorithms are oblivious shout algorithms
and run on anonymous networks as well.

\keywords{broadcast, synchronous
distributed systems, complete graph, dynamic edge failure, fault
tolerance}
