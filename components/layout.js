import Head from 'next/head'
import Link from 'next/link'
import { useState } from 'react';

export const siteTitle = 'OPODIS 2021'

export default function Layout({ children, home }) {
  const [menuIsOpen, setMenuOpen] = useState(false);

  return (
    <>
        <Head>
            <meta name="og:title" content={siteTitle} />
            <link rel="icon" href="/favicon.png" />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic" />
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet"></link>
        </Head>
        <div className="container">
            <main>{children}</main>
        </div>
        <footer>
            <div className="container">
                
            </div>
        </footer>
    </>
  )
}


