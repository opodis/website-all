import Head from 'next/head'
import Link from 'next/link'
import Layout from '../components/layout'

import links from '../link-to-websites.yaml';
import committee from '../committee.yaml';

export default function Home() {

  let current_year = Object.keys(links.next_edition)[0];
  let current_info = Object.values(links.next_edition)[0];

  return (
      <Layout>
        <section id="information">
          <div className="row">
            <h2>About OPODIS</h2>
          </div>
          <div className="row">
            <div className="column">
              <div>
                  <b>OPODIS </b>
                  is an open forum for the exchange of state-of-the-art knowledge concerning distributed computing and distributed computer systems. All aspects of distributed systems are within the scope of OPODIS, including theory, specification, design, performance, and system building. With strong roots in the theory of distributed systems, OPODIS now covers the whole range between the theoretical aspects and practical implementations of distributed systems, as well as experimentation and quantitative assessments.
              </div>
            </div>
          </div>
        </section>
        <section id="committee">
          <div className="row">
          <h2>Steering Committee</h2>
          </div>
          <div className="row">
            <ul>
              {
              committee.current.map(member => <li>{member}</li>)
              }
            </ul>
          </div>
        </section>
        <section id="next-edition">
          <div className="row">
            <h2>Next edition</h2>
          </div>
          {current_info.url && current_info.url.indexOf('.') != -1
          ? <div className="row">
             <a href={current_info.url}>OPODIS {current_year}</a>, {current_info.place}, {current_info.date}
            </div>
          : <div className="row">
              Soon: OPODIS {current_year}, {current_info.place}, {current_info.date}
            </div>
          }
        </section>
        <section id="prev-edition">
          <div className="row">
            <h2>Previous editions</h2>
          </div>
          <div className="row">
            <ul>
              {Object.entries(links.previous_editions)
              .sort((e1, e2) => e2[0] - e1[0])
              .map(([year, info]) => 
              <li>
                {
                  info.url 
                  ? <><a href={info.url}>OPODIS {year}</a> {info.dead && '(dead link)'}, </>
                  : <span>OPODIS {year}, </span>
                  }
                {info.place}, {info.date} <a href={info.dblp || `https://dblp.org/db/conf/opodis/opodis${year}.html`}>dblp</a>
              </li>)}
            </ul>
          </div>
        </section>
    </Layout>
  )
}
